import numpy as np
import copy
import time
import cv2

np.set_printoptions(threshold=np.inf)
img = cv2.imread('/Users/zhuj/WORK_STUDY/Python/CNN_Demo/static/demo_2.png')
b, g, r = cv2.split(img)

B = np.array(b)

print(B)

copy_pic1 = copy.deepcopy(B)

# edge detection

kernal = np.array([[-1, -1, -1],
                   [-1, 8, -1],
                   [-1, -1, -1]
                   ])

print(copy_pic1.shape)

converluted_area = np.zeros(shape=(3, 3))
final_area = np.zeros(shape=(1519, 2026))

for m in range(0, (copy_pic1.shape[0] - 2)):
    for n in range(0, (copy_pic1.shape[1] - 2)):
        sum = 0
        converluted_area = np.zeros(shape=(3, 3))

        converluted_area[0][0] = copy_pic1[m][n]
        sum = sum + converluted_area[0][0] * kernal[0][0]

        converluted_area[0][1] = copy_pic1[m][n + 1]
        sum = sum + converluted_area[0][1] * kernal[0][1]

        converluted_area[0][2] = copy_pic1[m][n + 2]
        sum = sum + converluted_area[0][2] * kernal[0][2]

        converluted_area[1][0] = copy_pic1[m + 1][n]
        sum = sum + converluted_area[1][0] * kernal[1][0]

        converluted_area[1][1] = copy_pic1[m + 1][n + 1]
        sum = sum + converluted_area[1][1] * kernal[1][1]

        converluted_area[1][2] = copy_pic1[m + 1][n + 2]
        sum = sum + converluted_area[1][2] * kernal[1][2]

        converluted_area[2][0] = copy_pic1[m + 2][n]
        sum = sum + converluted_area[2][0] * kernal[2][0]

        converluted_area[2][1] = copy_pic1[m + 2][n + 1]
        sum = sum + converluted_area[2][1] * kernal[2][1]

        converluted_area[2][2] = copy_pic1[m + 2][n + 2]
        sum = sum + converluted_area[2][2] * kernal[2][2]
        final_area[m][n] = sum

print("the example")
print(final_area)


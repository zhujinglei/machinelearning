import numpy as np
import copy
import time

import os

pic_code = np.array([
    [255, 255, 255, 255, 255, 255, 255, 255, 132, 1],
    [255, 255, 255, 255, 255, 255, 252, 101, 0, 0],
    [255, 255, 255, 255, 255, 251, 89, 0, 0, 0],
    [255, 255, 255, 255, 249, 80, 0, 0, 0, 97],
    [255, 255, 255, 246, 70, 0, 0, 0, 116, 254],
    [255, 255, 249, 75, 0, 0, 0, 126, 255, 255],
    [255, 252, 85, 0, 0, 0, 128, 255, 255, 255],
    [255, 103, 0, 0, 0, 118, 255, 255, 255, 255],
    [135, 0, 0, 0, 111, 255, 255, 255, 255, 255],
    [1, 0, 0, 97, 254, 255, 255, 255, 255, 255]
])

copy_pic1 = copy.deepcopy(pic_code)

# edge detection

kernal = np.array([[-1, -1, -1],
                   [-1, 8, -1],
                   [-1, -1, -1]
                   ])

print(kernal[0][0])

print(kernal.shape[1])

converluted_area = np.zeros(shape=(3, 3))
final_area = np.zeros(shape=(8, 8))


for m in range(0, (copy_pic1.shape[0] - 2)):
    for n in range(0, (copy_pic1.shape[1] - 2)):
        sum = 0
        converluted_area = np.zeros(shape=(3, 3))

        converluted_area[0][0] = copy_pic1[m][n]
        sum = sum + converluted_area[0][0] * kernal[0][0]
        print("the current sum is : ", sum)
        print()
        print("the current extract area is: ")
        print(converluted_area)
        time.sleep(0.1)

        converluted_area[0][1] = copy_pic1[m][n + 1]
        sum = sum + converluted_area[0][1] * kernal[0][1]
        print("the current sum is : ", sum)
        print()
        print("the current extract area is: ")
        print(converluted_area)
        time.sleep(0.1)

        converluted_area[0][2] = copy_pic1[m][n + 2]
        sum = sum + converluted_area[0][2] * kernal[0][2]
        print("the current sum is : ", sum)
        print()
        print(converluted_area)
        time.sleep(0.1)

        converluted_area[1][0] = copy_pic1[m + 1][n]
        sum = sum + converluted_area[1][0] * kernal[1][0]
        print("the current sum is : ", sum)
        print()
        print("the current extract area is: ")
        print(converluted_area)
        time.sleep(0.1)

        converluted_area[1][1] = copy_pic1[m + 1][n + 1]
        sum = sum + converluted_area[1][1] * kernal[1][1]
        print("the current sum is : ", sum)
        print()
        print("the current extract area is: ")
        print(converluted_area)
        time.sleep(0.1)

        converluted_area[1][2] = copy_pic1[m + 1][n + 2]
        sum = sum + converluted_area[1][2] * kernal[1][2]
        print("the current sum is : ", sum)
        print()
        print("the current extract area is: ")
        print(converluted_area)
        time.sleep(0.1)

        converluted_area[2][0] = copy_pic1[m + 2][n]
        sum = sum + converluted_area[2][0] * kernal[2][0]
        print("the current sum is : ", sum)
        print()
        print("the current extract area is: ")
        print(converluted_area)
        time.sleep(0.1)

        converluted_area[2][1] = copy_pic1[m + 2][n + 1]
        sum = sum + converluted_area[2][1] * kernal[2][1]
        print("the current sum is : ", sum)
        print()
        print("the current extract area is: ")
        print(converluted_area)
        time.sleep(0.1)

        converluted_area[2][2] = copy_pic1[m + 2][n + 2]
        sum = sum + converluted_area[2][2] * kernal[2][2]
        print("the current sum is : ", sum)
        print()
        print("the current extract area is: ")
        print(converluted_area)
        time.sleep(0.1)

        print("the completed converluted area is: ")
        print(converluted_area)
        print("the kernal is: ")
        print(kernal)
        print("the sum is ")
        final_area[m][n] = sum
        print("the final results after this converlution")
        print()
        print(final_area)
        time.sleep(2)


# for n in range(0, (copy_pic1.shape[0])):
#     for m in range(0, (copy_pic1.shape[1])):
#         if n > (kernal.shape[0] - 1):
#             i = n % kernal.shape[0]
#         else:
#             i = n
#
#         if m > (kernal.shape[1] - 1):
#             j = m % kernal.shape[0]
#         else:
#             j = m
#
#         copy_pic1[n][m] = copy_pic1[n][m] * kernal[i][j]
#         print(copy_pic1)
#         print("")
#         print("")
#         time.sleep(1)
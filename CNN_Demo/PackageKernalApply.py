import numpy as np
import scipy
from skimage import io, color
from skimage import exposure
import matplotlib.pyplot as plt



img = io.imread('/Users/zhuj/WORK_STUDY/Python/CNN_Demo/static/demo_2.png')  # Load the image

img = color.rgb2gray(img)  # Convert the image to grayscale (1 channel)
# apply sharpen filter to the original image
sharpen_kernel = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]])
image_sharpen = scipy.signal.convolve2d(img, sharpen_kernel, 'valid')
# apply edge detection filter to the sharpen image
edge_kernel = np.array([[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]])
edges = scipy.signal.convolve2d(image_sharpen, edge_kernel, 'valid')

# apply blur filter to the edge detection filtered image
blur_kernel = np.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]]) / 9.0;
denoised = scipy.signal.convolve2d(edges, blur_kernel, 'valid')


# Adjust the contrast of the filtered image by applying Histogram Equalization
denoised_equalized = exposure.equalize_adapthist(denoised / np.max(np.abs(denoised)), clip_limit=0.03)
plt.imshow(denoised_equalized, cmap=plt.cm.gray)  # plot the denoised_clipped
plt.axis('off')
plt.show()

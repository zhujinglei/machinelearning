import numpy as np
import pandas as pd
import csv

# reader = open('s1_t1.txt')
# list_data = reader.readlines()
# columns = list_data [0].split()
# list = []
# for i in list_data [1:]:
#     list.append(i.split())
# with open("test_1.csv","w+") as csvfile:
#     writer = csv.writer(csvfile)
#     #先写入columns_name
#     writer.writerow(columns)
#     #写入多行用writerows
#     writer.writerows(list)
#     writer.writerows('\n')

# txt_file = "s1_t1.txt"
# csv_file = "train.csv"
# in_txt = csv.reader(open(txt_file, "r"), delimiter = ']',escapechar='\n')
# out_csv = csv.writer(open(csv_file, 'w'))
# out_csv.writerows(in_txt)


# attention! 设置newline，否则会出现两行之间有一行空行
# with open('result.csv', 'w',newline = '', encoding = 'utf-8') as csvfile:
#     writer = csv.writer(csvfile)                 #writer为csv文件写入对象
#     data = open('s1_t1.txt', encoding='utf-8')  # txt文件内容存入data中
#     for each_line in data:
#         a = each_line.split(']'+',') # 把txt每一行的数据转换为list，split括号里的内容根据txt文件中每行各字符串的间隔符而定
#         writer.writerow('\n')
#         writer.writerow(a)

# result=[]
#
# with open('s1_t1.txt','r') as f:
#     for line in f:
#         result.append(list(map(float and str,line.split(']'+','))))
#         print(result)


# f = open('s1_t1.txt', 'r')
#
# print(f.read())

#a = np.loadtxt('s1_t1.txt')
#
# import csv
#
# txt_file = "try_1.txt"
# csv_file = "train.csv"
# in_txt = csv.reader(open(txt_file, "r"), delimiter = ']' and ',',escapechar= '\n' and '[' )
# out_csv = csv.writer(open(csv_file, 'w'))
# out_csv.writerows(in_txt)


# name = ['traffic', 'time']
# test = pd.DataFrame(columns=name, data=list)  # 数据有三列，列名分别为one,two,three
# print(test)
# test.to_csv('test_1', encoding='gbk')

# def read_data(dir_str):
#     '''
#     此函数读取txt文件中的数据
#     数据内容：科学计数法保存的多行两列数据
#     输入：txt文件的路径
#     输出：小数格式的数组，行列与txt文件中相同
#     '''
#     data_temp=[]
#     with open(dir_str) as fdata:
#         while True:
#             line=fdata.readline()
#             if not line:
#                 break
#             data_temp.append([float(i) for i in line.split()])
#     return np.array(data_temp）

# f = open('try_1.txt', 'r')
#print (f.read())
# data=f.read()
# name = ['traffic', 'time']
# test = pd.DataFrame(columns=name, data=data)
# print(test)
# test.to_csv('test_2', encoding='gbk')

# import ast
# #
# # with open('try_1.txt', 'r') as f:
# #     mylist = ast.literal_eval(f.read())

#
# data=np.genfromtxt('try_1.txt',delimiter=']')
# print(data)
#
# for i in data:
#     print(i)

# import pickle
#
# with open('try_1.txt', 'rb') as f:
#     my_list = pickle.load(f)

import re
import csv
from itertools import zip_longest
import itertools

import pandas as pd
from datetime import datetime
fname = 'anomaly_data_z1.txt'

with open(fname, 'r', encoding='utf-8') as f:  # 打开文件
    lines = f.readlines()  # 读取所有行
    first_line = lines[1]

def clean_data(rawdata):
    a=rawdata.replace("[",'')
    b=a.replace('"','')
    c=b.replace(':',',')
    d=c.replace('[','')
    e=d.replace(']','')
    f=e.replace('{','')
    g=f.replace('}','')
    final_str = g.strip().split(',')
    return final_str

list=clean_data(first_line)

list_time= list[::2]
list_time_formated=[]
list_times=list_time.pop(0)

for i in range(len(list_time)):
    if i==0:
        list_time_formated.append(list_time[i])
    else:
        list_time_formated.append(datetime.fromtimestamp(float(list_time[i])).strftime('%Y-%m-%d %H:%M:%S'))

list_amount=list[1::2]

# #print(list_amount)
# #print(list_time_formated)
dataframe = pd.DataFrame({'time':list_time_formated,'amount':list_amount})
# #print(datetime.fromtimestamp(int("1513722600")).strftime('%Y-%m-%d %H:%M:%S'))
# # with open("test1.csv", 'w') as f:
dataframe.to_csv("test0.csv",index=False,sep=',',header=None)

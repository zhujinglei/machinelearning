import re
import csv
from itertools import zip_longest
import itertools

# Clean the data and put the sites information into the csv file for further analysis
# Covert them into data frame for ease of analyse

import pandas as pd
from datetime import datetime
fname = '/Users/zhuj/WORK_STUDY/Machine_Learning/winter_exercise/data/anomaly_data_z1.txt'

with open(fname, 'r', encoding='utf-8') as f:  # 打开文件
    lines = f.readlines()  # 读取所有行
    first_line = lines[0]

def clean_data(rawdata):
    a=rawdata.replace("[",'')
    b=a.replace('"','')
    c=b.replace(':',',')
    d=c.replace('[','')
    e=d.replace(']','')
    f=e.replace('{','')
    g=f.replace('}','')

    final_str = g.strip().split(',')
    return final_str

list=clean_data(first_line)

list_time= list[::2]
list_time_formated=[]
list_time.pop(0)

for i in range(len(list_time)):
    if i==0:
        list_time_formated.append(list_time[i])
    else:
        list_time_formated.append(datetime.fromtimestamp(float(list_time[i])).strftime('%Y-%m-%d %H:%M:%S'))

list_amount=list[1::2]

#print(list_amount)
#print(list_time_formated)




dataframe = pd.DataFrame({'time':list_time_formated,'amount':list_amount})

#print(datetime.fromtimestamp(int("1513722600")).strftime('%Y-%m-%d %H:%M:%S'))
# with open("test1.csv", 'w') as f:

dataframe.to_csv("/Users/zhuj/WORK_STUDY/Machine_Learning/winter_exercise/data/datatest1.csv",index=False,sep=',',header=True)


print(dataframe)
print(type(dataframe))


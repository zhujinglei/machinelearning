import csv
import numpy as np
import pandas as pd
import random

# run simple anormaly detection - checking the input site information

# decide the threashold, here i use + - std (68%) as the example, after checking the distribution of the parameters (assuming
# normal distribution)
# if the incoming variable have been flag up, go to R for further analysis!



def read_site_info():
    df=pd.read_csv("data/site1_1.csv")
    print(df.head(10))
    return df

def calculate_average(df, colomnn_name):
    x=df[colomnn_name].mean()
    return x

def cal_std_floor(df,colomn,avg):
    sd = np.std(df[colomn])
    floor = avg - 1 * sd
    return floor

def cal_std_ceil(df,colomn,avg):
    sd = np.std(df[colomn])
    ceil = avg + 1 * sd
    return ceil

def detect_signal(dict_border,sample_dict,column_list):

    for char in column_list:
        select_matrix = dict_border[char]
        if sample_dict[char] >select_matrix[1] or sample_dict[char] < select_matrix[2]:
            print(char,'--'*10,'\x1b[1;30;42m'+'Alert'+'\x1b[0m')
        else:
            print(char,'--'*10,'Safe')


# def generating_map():
#
#     plt.gcf().set_facecolor(np.ones(3)) * 240 / 255
#     plt.grid()
#     plt.show()


if __name__ == "__main__":

    sample_dict={}
    df=read_site_info()
    column_list = list(df)
    column_list.pop(0)
    column_list.pop(0)
    dict_border={}
    for char in column_list:
        try:
            ave=calculate_average(df,char)
            ceil = cal_std_ceil(df, char, ave)
            floor = cal_std_floor(df, char, ave)
            dict_border[char]=ave,ceil,floor
            sample_dict[char]=ave*(1+random.uniform(-1.5,1.5))
        except:
            print('NA')

    print('The Boundary : Avg, Ceil, Floor ',dict_border)
    print('The Input Data Example : Amount',sample_dict)
    print(' ')
    print('Results')
    detect_signal(dict_border,sample_dict,column_list)


    # generating_map()





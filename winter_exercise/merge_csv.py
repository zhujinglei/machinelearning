import pandas as pd
from datetime import datetime
fname = '/Users/zhuj/WORK_STUDY/Machine_Learning/winter_exercise/data/anomaly_data_z1.txt'


def clean_data(rawdata):
    a=rawdata.replace('[','')
    b=a.replace('"','')
    c=b.replace(':',',')
    d=c.replace('[','')
    e=d.replace(']','')
    f=e.replace('{','')
    g=f.replace('}','')
    final_str = g.strip().split(',')
    return final_str


def read_lines(line):
    with open(fname, 'r', encoding='utf-8') as f:  # 打开文件
        lines = f.readlines()  # 读取所有行
        line_read = lines[line]
    return line_read


def list_generated(list):

    list_time= list[::2]
    list_time_formated=[]
    list_time.pop(0)

    for i in range(len(list_time)):
        if i==0:
            list_time_formated.append(list_time[i])
        else:
            list_time_formated.append(datetime.fromtimestamp(float(list_time[i])).strftime('%Y-%m-%d %H:%M:%S'))

    list_amount=list[1::2]
    return list_amount, list_time_formated


def data_frame_generation(list_time_formated, list_amount):

    dataframe = pd.DataFrame({'time':list_time_formated,'amount':list_amount})
    print(dataframe)


if __name__ == "__main__":

    # execute only if run as a script

    data_frame_dict = {}

    for i in range(78):
        rawdata=read_lines(i)
        list=clean_data(rawdata)
        list_amount,list_time_formated = list_generated(list)
        dataframe = pd.DataFrame({'time': list_time_formated, 'amount': list_amount})
        data_frame_dict[str(list_amount[0])]=dataframe

        print(data_frame_dict)
        if i == 0:
            dataframe.to_csv("/Users/zhuj/WORK_STUDY/Machine_Learning/winter_exercise/data/datatest10.csv", index=True,
                             sep=',', header=True)
        else:
                df=pd.read_csv("/Users/zhuj/WORK_STUDY/Machine_Learning/winter_exercise/data/datatest10.csv")
                pd.concat([df,dataframe], axis=1).to_csv("/Users/zhuj/WORK_STUDY/Machine_Learning/winter_exercise/data/datatest10.csv",header=True,index=False)